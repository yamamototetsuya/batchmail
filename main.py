# coding: utf-8

from batchmail.BatchInfo import BatchInfo
from batchmail.BatchSender import BatchSender
from batchmail.gui import BatchMailGUI

if __name__ == '__main__':
  bi = BatchInfo()
  #bi.load_csv('sample.csv')

  mailsender = BatchSender('config.txt')

  #mailsender.send_mail_by_batch_info_all(bi)

  BatchMailGUI(bi, mailsender)

