# coding: utf-8

import sys
import traceback
import Tkinter as tk
import tkMessageBox
import tkFileDialog
import time
import os.path
import ConfigParser


help_msg=u"""■操作説明■

送りたいメールについての情報を、CSVとして準備しておきます。
現在は、Shift JISのみの文字エンコードをサポートします。
Excelで出力したCSVとの相性がよいように作っています。
列の数はいくつでもよいですが、最初の列には送信先の
メールアドレスを入れておくのが普通です。

そうしたら、次に、メールタイトルやメール本文の雛形を編集します。
{%1} という記号は、CSVの中身で置き換えられます。この場合は
左から二番目の列の値です。
同様に、{%2}は三番目の列、{%3}には四番目の列・・・という具合に
続きます。
{%0}はメールアドレスが入ります。宛先emailはこのひな型を
入れておくのが普通です。

メールがうまく生成されているかは、メニューの[確認モード]を使って
確認しましょう。確認中は、[前][次]のボタンで、データの行数分だけ
実際のメールの生成の様子を見られます。

添付ファイルは、メッセージ本文中に @ で始まる行を書くと作れます。
例：@ C:\\attach\{%1}.pdf

すべてうまくいきそうなら、[送信開始]をクリックして、あとは
メッセージを眺めながら処理が終わるのを待ちましょう。

実際のメール送信は、config.txtでの設定に従って行います。
"""


class BatchMailGUI(object):

  tpos = 0
  mode = "edit"
  t_area = None

  def __init__(self, batchinfo, mailsender):
    self.bi = batchinfo
    self.ms = mailsender
    self.tp = tk.Tk()
    tp = self.tp
    tp.wm_title(u'メール送信ツール')
    tp.wm_minsize(width=320, height=150) #最小サイズ指定

    self.safety_switch = tk.BooleanVar(value=True)
    self.recipientname = tk.StringVar(value=self.bi.recipientname)
    self.recipientmail = tk.StringVar(value=self.bi.recipientmail)
    self.sendername = tk.StringVar(value=self.bi.sendername)
    self.sendermail = tk.StringVar(value=self.bi.sendermail)
    self.subject = tk.StringVar(value=self.bi.title)

    row = tk.Frame(tp)
    for i in [
      tk.Button(row, text=u'CSV取込', command=self.load_csv),
      tk.Button(row, text=u'編集モード', command=self.mode_edit),
      tk.Button(row, text=u'確認モード', command=self.mode_show),
      tk.Button(row, text=u'ヘルプ', command=self.mode_help),
      tk.Button(row, text=u'終了', command=tp.quit),
    ]:
      i.pack(side=tk.LEFT)
    row.pack(side=tk.TOP, expand=0, anchor=tk.W)


    row = tk.Frame(tp)
    for i in [
      tk.Label(row, text=u'送信者email'),
      tk.Entry(row, width=50, textvariable=self.sendermail),
      tk.Label(row, text=u'送信者名'),
      tk.Entry(row, width=34, textvariable=self.sendername),
    ]:
      i.pack(side=tk.LEFT)
    row.pack(side=tk.TOP, expand=0, anchor=tk.W)


    row = tk.Frame(tp)
    for i in [
      tk.Label(row, text=u'宛先email'),
      tk.Entry(row, width=50, textvariable=self.recipientmail),
      tk.Label(row, text=u'宛先名'),
      tk.Entry(row, width=34, textvariable=self.recipientname),
    ]:
      i.pack(side=tk.LEFT)
    row.pack(side=tk.TOP, expand=0, anchor=tk.W)


    row = tk.Frame(tp)
    for i in [
      tk.Label(row, text=u'タイトル'),
      tk.Entry(row, width=60, textvariable=self.subject),
    ]:
      i.pack(side=tk.LEFT)
    row.pack(side=tk.TOP, expand=0, anchor=tk.W)


    row = tk.Frame(tp)
    self.sendbutton = tk.Button(row, text=u'送信開始', command=self.mode_send)
    for i in [
      tk.Label(row, text=u'メール確認'),
      tk.Button(row, text=u'前', command=self.go_left),
      tk.Button(row, text=u'次', command=self.go_right),
      tk.Frame(row, width=10),
      self.sendbutton,
    ]:
      i.pack(side=tk.LEFT)
    row.pack(side=tk.TOP, expand=0, anchor=tk.W)


    row = tk.Frame(tp)
    self.t_area = tk.Text(row)
    _t = self.t_area
    sb1 = tk.Scrollbar(row, orient='v', command = _t.yview)
    _t.configure(yscrollcommand = sb1.set)
    _t.pack(side=tk.LEFT, expand=1, fill=tk.BOTH)
    sb1.pack(side=tk.LEFT, expand=0, fill=tk.Y)
    row.pack(side=tk.TOP, expand=1, fill=tk.BOTH)


    row = tk.Frame(tp)
    for i in [
      tk.Checkbutton(row, text=u'安全装置（メールを実際には送らない）', variable=self.safety_switch),
    ]:
      i.pack(side=tk.LEFT)
    row.pack(side=tk.TOP, expand=0, anchor=tk.W)


    self.mode_edit()
    tp.mainloop()

  def load_csv(self):
    filename = tkFileDialog.askopenfilename()
    if filename != "":
      self.bi.load_csv(filename)
      self.tpos = 0
      self.mode_show()

  def setback_config(self):
    self.bi.set_sender(self.sendermail.get(), self.sendername.get())
    self.bi.set_recipient(self.recipientmail.get(), self.recipientname.get())
    self.bi.set_title(self.subject.get())
    self.ms.safety_switch = self.safety_switch.get()
    if self.mode == 'edit':
      self.bi.set_message(self.t_area.get('1.0', tk.END).strip())

  def clear_msg(self):
    self.t_area.delete('1.0', tk.END)

  def show_msg(self, msg):
    self.t_area.insert(tk.END, msg + "\n")
    self.t_area.yview_moveto(1.0)

  def mode_send(self):

    self.setback_config()
    self.mode = "send"
    self.t_area.configure(bg='#ffdddd', state=tk.NORMAL)
    self.clear_msg()
    self.sendbutton.configure(state=tk.DISABLED)
    self.show_msg(u'■送信開始！')
    self.tp.wm_title(u'メール送信ツール(送信中・・・)\n')

    self.ms.set_message_callback(self.show_msg)
    self.gen = self.ms.send_mail_by_batch_info(self.bi)
    self.tp.after(100, self.do_progress)

    #for i in self.ms.send_mail_by_batch_info(self.bi):
    #  pass

  def do_progress(self):
    try:
      self.gen.next()
      self.tp.after(100, self.do_progress)
    except StopIteration:
      self.show_msg(u'■送信終了！')
      self.sendbutton.configure(state=tk.NORMAL)

  def show_progress(self, msg):
    self.t_area.configure(bg='#ffdddd', state=tk.NORMAL)
    self.show_msg(msg)


  def mode_help(self):
    self.setback_config()
    self.mode = "help"
    self.t_area.configure(bg='#ddddff', state=tk.NORMAL)
    self.clear_msg()
    self.show_msg(help_msg)
    self.tp.wm_title(u'メール送信ツール(ヘルプ)')

  def mode_edit(self):
    #self.setback_config()
    if self.mode != "edit":
      self.mode = "edit"
    self.t_area.configure(bg='#ffffff', state=tk.NORMAL)
    self.clear_msg()
    self.show_msg(self.bi.rawmessage)
    self.tp.wm_title(u'メール送信ツール(編集モード)')

  def show_mail_1(self):
    self.t_area.configure(state=tk.NORMAL)
    self.clear_msg()
    self.show_msg(self.bi.get_message_summary(self.tpos))
    self.t_area.yview_moveto(0.0)
    self.t_area.configure(bg='#eeee88', state=tk.DISABLED)
    self.tp.wm_title(u'メール送信ツール(確認モード) %d/%d' % (self.tpos+1, len(self.bi.records)))

  def mode_show(self):
    self.setback_config()
    self.mode = "show"
    self.show_mail_1()

  def go_right(self):
    self.mode_show()
    if self.tpos < len(self.bi.records)-1:
      self.tpos += 1
    self.show_mail_1()

  def go_left(self):
    self.mode_show()
    if self.tpos > 0:
      self.tpos -= 1
    self.show_mail_1()

