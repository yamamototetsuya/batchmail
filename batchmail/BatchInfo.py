# coding: utf-8

import sys
import traceback
import csv
import os.path
from email import encoders
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.nonmultipart import MIMENonMultipart
from email.header import Header

if 'win' in sys.platform:
  local_enc = 'cp932'
else:
  local_enc = 'utf-8'
to_enc = 'iso-2022-jp'
fallback_enc = 'utf-8'


mimedict = {
 '.xls': ('application', 'vnd.ms-excel'),
 '.xlsx': ('application', 'vnd.ms-excel'),
 '.jpg': ('image', 'jpg'),
 '.gif': ('image', 'gif'),
 '.pdf': ('applictaion', 'pdf'),
}
try:
  for line in open('mime.txt'):
    a = line[:-1].split("|")
    if len(a) >= 3:
      mimedict[a[0]] = (a[1], a[2])
except IOError:
  pass

# メールヘッダーをつくる
def for_header(s):
  try:
    s.encode('ascii')
    return s
  except UnicodeEncodeError:
    return Header(s, to_enc).__str__()

# テキストメールをあらわすメッセージオブジェクト
def text_msg(body):
  try:
    msg = body.encode(to_enc)
    msg = MIMEText(msg, 'plain', to_enc)
  except UnicodeError:
    msg = body.encode(fallback_enc)
    msg = MIMEText(msg, 'plain', fallback_enc)
  return msg

# 添付ファイルをあらわすメッセージオブジェクト
def attachment_msg(filepath):
  localfilepath = filepath.encode(local_enc)
  data = open(localfilepath, 'rb').read()
  fname = os.path.split(localfilepath)[1]
  fmime = mimedict.get(os.path.splitext(fname)[1], ('application', 'octet-stream'))
  encoded_filename = for_header(fname.decode(local_enc))
  msg = MIMENonMultipart(fmime[0], fmime[1], filename=encoded_filename)
  msg.set_payload(data)
  encoders.encode_base64(msg)
  msg.add_header("Content-Disposition", "attachment", filename=encoded_filename)
  return msg

# 添付メール全体をあらわすメッセージオブジェクト
def multipart_msg(from_email, to_email, title, message, attachments):
  msgs = [text_msg(message)]
  for a in attachments:
    try:
      msgs.append(attachment_msg(a))
    except IOError:
      pass
  if len(msgs) == 1:
    msg = msgs[0]
  else:
    msg = MIMEMultipart()
    for m in msgs:
      msg.attach(m)

  t = from_email.split(" ")
  msg['From'] = " ".join(map(for_header, t))
  t = to_email.split(" ")
  msg['To'] = " ".join(map(for_header, t))
  msg['Subject'] = for_header(title)
  return msg

class BatchInfo(object):

  def __init__(self):
    self.records = [['sample@sample.mail', 'sample data'],]
    self.rawmessage = 'this is the mail for {%1}.'
    self.message = 'this is the mail for {%1}.'
    self.title = 'sample mail'
    self.recipientmail = '{%0}'
    self.recipientname = ''
    self.sendermail = 'sender@sample.mail'
    self.sendername = ''
    self.attachments = []

  def set_message(self, message):
    self.rawmessage = message
    t = []
    self.attachments = []
    for line in message.split("\n"):
      if line.startswith("@ "):
        self.attachments.append(line[2:])
      else:
        t.append(line)
    self.message = "\n".join(t)

  def set_title(self, title):
    self.title = title

  def set_recipient(self, recipientmail, recipientname=''):
    self.recipientmail = recipientmail
    self.recipientname = recipientname

  def get_recipientstring(self):
    if self.recipientname:
      return '%s <%s>' % (self.recipientname, self.recipientmail)
    else:
      return '%s' % (self.recipientmail)

  def set_sender(self, sendermail, sendername=''):
    self.sendermail = sendermail
    self.sendername = sendername

  def get_senderstring(self):
    if self.sendername:
      return '%s <%s>' % (self.sendername, self.sendermail)
    else:
      return '%s' % (self.sendermail)

  def get_replaced(self, tmpl, values):
    s = tmpl
    for i in xrange(len(values)):
      s = s.replace('{%%%d}' % i, values[i])
    return s

  def get_message_dict(self, index):
    d = {}
    d['sender'] = self.get_replaced(self.sendermail, self.records[index])
    d['recipient'] = self.get_replaced(self.recipientmail, self.records[index])
    d['senderstring'] = self.get_replaced(self.get_senderstring(), self.records[index])
    d['recipientstring'] = self.get_replaced(self.get_recipientstring(), self.records[index])
    d['subject'] = self.get_replaced(self.title, self.records[index])
    d['message'] = self.get_replaced(self.message, self.records[index])
    d['attachments'] = [self.get_replaced(a, self.records[index]) for a in self.attachments]
    return d

  def get_message_summary(self, index):
    t = []
    d = self.get_message_dict(index)
    t.append("From: " + d['senderstring'])
    t.append("To: " + d['recipientstring'])
    t.append("Subject: " + d['subject'])
    t.append('')
    t.append(d['message'])
    if d['attachments']:
      t.append('')
      for a in d['attachments']:
        if os.path.exists(a.encode(local_enc)):
          t.append('[[' + a + ']]')
        else:
          t.append('[[' + a + ' (not exists)]]')
    return "\n".join(t)

  def get_message_object(self, index):
    d = self.get_message_dict(index)
    msg = multipart_msg(d['senderstring'], d['recipientstring'], d['subject'], d['message'], d['attachments'])
    return {'msg':msg, 'from':d['sender'], 'to':d['recipient']}

  def generate_message_objects(self):
    for i in xrange(len(self.records)):
      yield self.get_message_object(i)

  def load_csv(self, filename):
    self.records = []
    for a in csv.reader(open(filename)):
      self.records.append([i.decode('cp932') for i in a])
