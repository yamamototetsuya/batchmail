# coding: utf-8

import traceback
import time
from email import encoders
import smtplib
import ConfigParser


def dummy_callback(msg):
  print msg

class BatchSender(object):

  def __init__(self, configfile):
    defaults = {'server':'127.0.0.1', 'port':'25', 'security':'plain', 'interval':'5', 'user':'user', 'pass':'pass', 'logfile':'log.txt', 'disconnect_everytime':'0'}
    c = ConfigParser.ConfigParser(defaults)
    c.read(configfile)
    if not c.has_section('smtp'):
      c.add_section('smtp')
    self.server = c.get('smtp', 'server')
    self.port = c.getint('smtp', 'port')
    self.security = c.get('smtp', 'security')
    self.interval = c.getint('smtp', 'interval')
    self.smtpuser = c.get('smtp', 'user')
    self.smtppass = c.get('smtp', 'pass')
    self.logfile = c.get('smtp', 'logfile')
    self.disconnect_everytime = c.get('smtp', 'disconnect_everytime')
    self.callback = dummy_callback
    #self.callback('ready')
    self.safety_switch = True

  def set_message_callback(self, callback):
    self.callback = callback

  def connect_server(self):
    if self.security == 'starttls':
      self.smtp = smtplib.SMTP(self.server, self.port)
      self.callback('connect %s:%s' % (self.server, self.port))
      self.smtp.starttls()
      self.callback('starttls')
      if self.smtpuser:
        self.smtp.login(self.smtpuser, self.smtppass)
        self.callback('login')
    elif self.security == 'ssl':
      self.smtp = smtplib.SMTP_SSL(self.server, self.port)
      self.callback('connect %s:%s' % (self.server, self.port))
      if self.smtpuser:
        self.smtp.login(self.smtpuser, self.smtppass)
        self.callback('login')
    else:
      self.smtp = smtplib.SMTP(self.server, self.port)
      self.callback('connect %s:%s' % (self.server, self.port))

  def disconnect_server(self):
    self.smtp.quit()
    self.callback('disconnect')

  def send_mail(self, fromaddr, toaddr, msg):
    self.smtp.sendmail(fromaddr, [toaddr], msg.as_string())
    self.callback('mail sent: %s' % toaddr)

  def send_mail_by_batch_info(self, rs):
    if self.safety_switch:
      self.callback('safe switch ON (no actual connecting server)')
    try:
      if not self.safety_switch:
        self.connect_server()
      yield
      for msg in rs.generate_message_objects():
        self.callback('message to %s ...' % msg['to'])
        #print msg
        if not self.safety_switch:
          self.send_mail(msg['from'], msg['to'], msg['msg'])
        of = open(self.logfile, 'a')
        of.write(msg['msg'].as_string())
        of.write("\n---------------\n")
        of.close
        if self.disconnect_everytime == '1' and (not self.safety_switch):
          self.disconnect_server()
        time.sleep(self.interval)
        if self.disconnect_everytime == '1' and (not self.safety_switch):
          self.connect_server()
        yield
      if not self.safety_switch:
        self.disconnect_server()
    except:
      self.callback(traceback.format_exc().decode('cp932'))

  def send_mail_by_batch_info_all(self, rs):
    for i in self.send_mail_by_batch_info(rs):
      pass
