# coding: utf-8

import sys
from batchmail.BatchInfo import BatchInfo
from batchmail.BatchSender import BatchSender
import argparse

parser = argparse.ArgumentParser(description='send many mails using template.')
parser.add_argument('--data', help='CSV file for filling template')
parser.add_argument('--config', help='SMTP confguration file (default: config.txt)', default='config.txt')
parser.add_argument('--template', help='Template for email')
parser.add_argument('--mime', help='MIME definition file(default: mime.txt)', default='mime.txt')

args = parser.parse_args()



if not args.data:
  print parser.print_help()
  sys.exit()

if not args.template:
  print parser.print_help()
  sys.exit()



bi = BatchInfo()
try:
  bi.load_csv(args.data)
except:
  print "ERROR: no datafile"

mailsender = BatchSender(args.config)





#mailsender.send_mail_by_batch_info_all(bi)
